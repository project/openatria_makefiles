;******************************************************************************
;                                Documentation
;******************************************************************************
; Make OpenAtria.com makefile
; http://drupal.org/project/openatria_makefiles 
;
; Description:
; A stub drush makefile to create an up-to-date OpenAtrium installation profile
; on a Pressflow core. The standard Open Atrium platform is extended with
; additional features for OpenAtria.com, and some modules will be overridden in
; sites/all (patches, dev/git versions, &c.)
;
; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatria-stub.make"
; Or create a new platform by calling this makefile using Aegir's web interface
; ('Makefile' field on node/add/platform):
;   "/var/aegir/makefiles/openatria_makefiles/stub-openatria.make"
; Better yet, point to the raw (blob_plain) display of latest git commit
; remotely, so you can keep an exact reference in Aegir:
;   e.g., http://drupalcode.org/project/openatria_makefiles.git/blob_plain/<most recent commit hash>:/stub-openatria.make

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                               Include files
;******************************************************************************

; Since we're including another stub makefile, we already get core and
; openatrium.profile
includes[drupal] = "stub-openatrium.make"

;******************************************************************************
;                            Patches / Overrides
;******************************************************************************


;******************************************************************************
;                            Additional features
;******************************************************************************
; N.B.: makefiles recursively included in these features will *not* override
; those in the profile or other previously run makefile. So when we need to
; override, we include them here.

; Atrium Scrum
projects[atrium_scrum][subdir] = "OpenAtria"
;projects[atrium_scrum][type] = "module"
;projects[atrium_scrum][download][type] = "git"
;projects[atrium_scrum][download][url] = "http://git.drupal.org/project/atrium_scrum.git"
projects[atrium_scrum][version] = "1.0-alpha6"

; Atrium Captchas
projects[atrium_captchas][version] = "1.0-alpha2"

; Atrium Moderated Groups
projects[atrium_moderated_groups][version] = "1.0-alpha2"

;******************************************************************************
;                                     End
;******************************************************************************
