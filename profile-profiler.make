;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A drush makefile for OpenAtrium profile.

; Instructions:
; Include this makefile in a stub makefile like so:
;   includes[openatrium] = "http://drupalcode.org/project/openatria_makefiles.git/blob_plain/fa7ae8edb3d7c26691657178fb69879c77b1eb15:/profile-openatrium.make"
; This makefile checks out the latest commit to git of Open Atrium. If you'd
; like to point to a specific release version add:
;   projects[openatrium][download][tag] = "6.x-1.2"

; /////////////////////////////// REMINDER ////////////////////////////////////
; Update stub makefiles after any commits altering this makefile, so as to 
; refer to the latest commit hash.

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                              Open Atrium profile
;******************************************************************************

projects[openatrium][type] = profile
projects[openatrium][download][type] = git
projects[openatrium][download][url] = http://git.drupal.org/sandbox/ergonlogic/1473484.git
projects[openatrium][download][branch] = "profiler"
projects[openatrium][destination-folder] = "openatrium"

;******************************************************************************
;                                   Patches
;******************************************************************************

; none

;******************************************************************************
;                                     End
;******************************************************************************
