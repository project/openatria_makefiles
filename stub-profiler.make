;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date OpenAtrium installation profile
; on a Pressflow core, or Drupal core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatrium-stub.make"
; Or create a new platform by calling this makefile using Aegir's web interface
; ('Makefile' field on node/add/platform):
;   "/var/aegir/makefiles/openatria_makefiles/stub-openatrium.make"
; Better yet, point to the raw (blob_plain) display of latest git commit
; remotely, so you can keep an exact reference in Aegir:
;   e.g., http://drupalcode.org/project/openatria_makefiles.git/blob_plain/<most recent commit hash>:/stub-openatrium.make

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
;includes[pressflow] = "http://drupalcode.org/project/openatria_makefiles.git/blob_plain/8af0fe0e4f5bc18afcaf38dc9c24ac26e44c5a8c:/core-pressflow.make"
;projects[pressflow][download][tag] = "6.25"
; OR get standard Drupal and set version
includes[drupal] = "http://drupalcode.org/project/openatria_makefiles.git/blob_plain/8a9455d61f95419ec7aa7cd23ab5d77c5e495f73:/core-drupal.make"
projects[drupal][download][tag] = "6.27"

includes[openatrium] = "http://drupalcode.org/project/openatria_makefiles.git/blob_plain/HEAD:/profile-profiler.make"
;projects[openatrium][download][tag] = "6.x-1.2"

;******************************************************************************
;                                  Patches
;******************************************************************************

; If using Pressflow, we probably need to override boxes.module. Ref.:
; http://drupal.org/node/887260#comment-3488192 and
; https://bugs.launchpad.net/pressflow/+bug/652027
; projects[boxes][version] = "1.1"

;******************************************************************************
;                                     End
;******************************************************************************
